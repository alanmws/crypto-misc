import logging
import sys

import pandas as pd
import numpy as np

from datetime import datetime, timezone
from exchanges import exchange_from_str

from telegram.ext import Updater
from apscheduler.schedulers.blocking import BlockingScheduler

telegram_token = "---"
telegram_chat_id = "1576164251" #mine (AS)

class AccountMonitor:

    def __init__(self, exchange, run_name: str, token_monitored: str, drawdown_threshold_pct: float):
        self.exchange = exchange
        self.run_name = run_name
        self.token_monitored = token_monitored
        self.drawdown_threshold_pct = drawdown_threshold_pct
        self._alerts_in_session = set()

    def check_drawdown(self, balances_df: pd.DataFrame):
        token_values = balances_df[self.token_monitored]
        max_token_index = token_values.argmax()
        max_token_value = token_values.iloc[max_token_index]
        values_post_max = token_values[max_token_index:]
        min_after_max_index = token_values[max_token_index:].argmin()
        min_after_max_value = values_post_max.iloc[min_after_max_index]

        drawdown_pct = 100 * (max_token_value - min_after_max_value) / max_token_value
        if drawdown_pct > self.drawdown_threshold_pct:
            # Notify the user
            alert_msg = "ALERT on {}: Max drawdown has been exceeded, reached {:.2f}% percent".format(self.run_name,
                                                                                                      drawdown_pct)
            if alert_msg not in self._alerts_in_session:
                self._alerts_in_session.add(alert_msg)
                print(alert_msg)
                telegram_updater = Updater(token=telegram_token)
                telegram_updater.bot.send_message(telegram_chat_id, text=alert_msg)


    def check_balance(self):
        time_now_utc = datetime.now(timezone.utc)
        filename_csv = self.run_name + '.csv'
        try:
            balances_df = pd.read_csv(filename_csv)
        except FileNotFoundError:
            first_entry = {'datetime': time_now_utc,
                           self.token_monitored: self.exchange.fetchBalance()['total'][self.token_monitored]}
            balances_df = pd.DataFrame([first_entry])
            balances_df.to_csv(filename_csv, index=False)
            print("Saved new file to CSV named: ", filename_csv)
            return
        new_row = {'datetime': time_now_utc,
                   self.token_monitored: self.exchange.fetchBalance()['total'][self.token_monitored]}

        starting_balance = balances_df.iloc[0][self.token_monitored]
        new_row['PNL%'] = (new_row[self.token_monitored] - starting_balance) / starting_balance

        self.check_drawdown(balances_df)

        print("New check, adding row: ", new_row)
        balances_df = balances_df.append(new_row, ignore_index=True)
        balances_df.to_csv(filename_csv, index=False)
        print("Updated CSV named: ", filename_csv)

if __name__ == "__main__":
    #logging.basicConfig(level=logging.DEBUG)

    telegram_token_test = "5720398463:AAEoJXK13U_6Tp66Xi_dMjsqdJd-iTwujgA"
    telegram_updater = Updater(token=telegram_token_test)
    telegram_updater.bot.send_message(telegram_chat_id, text="Testowo")

    sched = BlockingScheduler()

    assert len(sys.argv) == 1+5

    exchange = exchange_from_str(sys.argv[1])
    run_name = sys.argv[2]
    token = sys.argv[3]
    drawdown_threshold_pct = float(sys.argv[4])
    interval_sec = int(sys.argv[5])
    monitor = AccountMonitor(exchange, run_name, token, drawdown_threshold_pct)

    monitor.check_balance() # run immediately once, then every interval
    sched.add_job(monitor.check_balance, 'interval', seconds=interval_sec)

    print("Starting the scheduler...")
    sched.start()
