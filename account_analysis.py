import ccxt
import sys
import copy

import pandas as pd
import numpy as np
from datetime import datetime
from IPython.display import display, HTML

import plotly.express as px

from dataclasses import dataclass

@dataclass
class TradingPeriodPerformance:
    pnl_currency: str
    pnl_absolute: float
    pnl_relative_pct: float

def fetch_trades_since(exchange, filtered_symbol: str, date_since_ms: int):
    # Below refetching period is based on Binance, which will give only 7 days max of trades from the provided 'since' datetime
    refetch_every_days = 7
    refetch_every_miliseconds = refetch_every_days*24*3600*1e3 # 7 days
    print("Fetching trades...")
    if exchange.has['fetchTrades']:
        since = date_since_ms
        all_fetched_trades = []
        while since < exchange.milliseconds():
            period_end_ms = since + refetch_every_miliseconds
            print("Fetching for period of ", refetch_every_days, " days from ", datetime.fromtimestamp(since/1000.0))
            while since < exchange.milliseconds() and since < period_end_ms:
                limit = 50
                trades = exchange.fetchMyTrades(filtered_symbol, since, limit)
                if len(trades):
                    since = trades[-1]['timestamp'] + 1
                    all_fetched_trades += trades
                else:
                    break
            since += refetch_every_miliseconds
    else:
        raise Exception("Exchange does not have a method to fetch trades available")
    print("Fetched ", len(all_fetched_trades), " trades")

    # CHECK if really sorted by timestsamp
    assert (all(all_fetched_trades[i]['timestamp'] <= all_fetched_trades[i + 1]['timestamp'] for i in range(len(all_fetched_trades) - 1)))

    return all_fetched_trades

def fetch_balances_for_single_pair(exchange, base_symbol: str, quote_symbol: str):
    balance = exchange.fetchBalance()
    balance_symbols_used = [base_symbol, quote_symbol]
    current_balances = dict()
    for symbol in balance_symbols_used:
        if symbol not in balance['total']:
            current_balances[symbol] = 0
        else:
            current_balances[symbol] = balance['total'][symbol]
    print("Current balances: ", current_balances)
    return current_balances

def fetch_all_balances(exchange):
    print("Fetching all balances...")
    balance = exchange.fetchBalance()
    current_balances = dict()
    nonzero_threshold = 0.001
    for symbol in balance["total"]:
        if balance["total"][symbol] > nonzero_threshold:
            current_balances[symbol] = balance["total"][symbol]
    return current_balances

def backpropagate_trades(current_balances: dict, trades: list, quote_symbol: str, base_symbol: str=None):
    # Create first row, representing state after the last trade
    latest_trade_datetime = datetime.utcfromtimestamp(trades[-1]['timestamp'] / 1e3)
    first_row = {'datetime': latest_trade_datetime, "trade_id": trades[-1]['id']}
    first_row.update(current_balances)
    tradingperiod_list = [first_row]

    for i in range(len(trades)-1, -1, -1): # Loop backwards from latest trade
        trade = trades[i]
        trade_row_after = tradingperiod_list[-1]
        assert trade['symbol'] == base_symbol, "Symbol not expected: " + trade['symbol'] + " vs " + base_symbol # make sure symbol matches
        if trade['side'] =='buy':
            quote_delta = -trade['cost'] # price * amount
            base_delta = trade['amount']
        elif trade['side'] == 'sell':
            quote_delta = trade['cost'] # price * amount
            base_delta = -trade['amount']
        else:
            raise Exception("trade side is not buy or sell")
        trade_datetime = datetime.utcfromtimestamp(trade['timestamp']/1e3)
        quote_amount = trade_row_after[quote_symbol] - quote_delta
        if base_symbol:
            base_amount = trade_row_after[base_symbol] - base_delta
            trade_row = {'datetime': trade_datetime, quote_symbol: quote_amount, base_symbol: base_amount, 'trade_id': trade['id']}
        else:
            trade_row = {'datetime': trade_datetime, quote_symbol: quote_amount, 'trade_id': trade['id']}
        tradingperiod_list.append(trade_row)
    return tradingperiod_list

def get_traded_symbols(trade):
    """
    Returns traded symbols
    :return: List of 2 symbols (base / derivative name first, quote/margin currency second)
    """
    if trade['info']['baseCurrency'] is not None:  # spot
        base_symbol = trade['info']['baseCurrency']
        quote_symbol = trade['info']['quoteCurrency']
    else:  # derivative
        base_symbol = trade['symbol']
        quote_symbol = trade['info']['feeCurrency']  # assuming fee currency is the margin currency/symbol
    return [base_symbol, quote_symbol]

def backpropagate_account_state(current_balances: dict, trades: list):
    symbols_traded = set()
    for trade in trades:
        symbols_traded |= set(get_traded_symbols(trade))
    print("Symbols that have been traded: ", symbols_traded)

    # Create first row, representing state after the last trade
    latest_trade_datetime = datetime.utcfromtimestamp(trades[-1]['timestamp'] / 1e3)
    first_row = {'datetime': latest_trade_datetime, "trade_id": trades[-1]['id']}
    for symbol in symbols_traded:
        if symbol not in current_balances:
            first_row[symbol] = 0.0
        else:
            first_row[symbol] = current_balances[symbol]
    tradingperiod_list = [first_row]

    for i in range(len(trades)-1, -1, -1): # Loop backwards from latest trade
        try:
            trade = trades[i]
            trade_row_after = tradingperiod_list[-1]
            if trade['side'] =='buy':
                quote_delta = -trade['cost'] # price * amount
                base_delta = trade['amount']
            elif trade['side'] == 'sell':
                quote_delta = trade['cost'] # price * amount
                base_delta = -trade['amount']
            else:
                raise ValueError("trade side is not buy or sell")
            if trade['info']['baseCurrency'] is not None:   # spot
                base_symbol = trade['info']['baseCurrency']
                quote_symbol = trade['info']['quoteCurrency']
            else:                                           # derivative
                base_symbol = trade['symbol']
                quote_symbol = trade['info']['feeCurrency'] # assuming fee currency is the margin currency/symbol

            trade_datetime = datetime.utcfromtimestamp(trade['timestamp']/1e3)
            if not quote_symbol in trade_row_after:
                trade_row_after[quote_symbol] = 0.0
            quote_amount = trade_row_after[quote_symbol] - quote_delta
            if not base_symbol in trade_row_after:
                trade_row_after[base_symbol] = 0.0
            base_amount = trade_row_after[base_symbol] - base_delta
            trade_row_before = copy.copy(trade_row_after)
            trade_row_before.update({'datetime': trade_datetime, quote_symbol: quote_amount, base_symbol: base_amount, 'trade_id': trade['id']})
            tradingperiod_list.append(trade_row_before)

        except Exception as e:
            print("Error occured on trade no ", i, ", trade is: ")
            print("trade_row_after: ", trade_row_after)
            display(trade)
            raise e

    return pd.DataFrame(tradingperiod_list)

def get_active_derivative_positions(exchange):
    positions = exchange.fetchPositions()
    positions_dict = {}
    for position in positions:
        position_amount = position['contracts']*position['contractSize']
        if position_amount == 0.0:
            continue
        positions_dict[position['symbol']]['side'] = position['side']
        positions_dict[position['symbol']]['amount'] = position_amount
    return positions_dict

def backpropagate_derivative_account_state(exchange, trades: list):
    # Create first row, representing state after the last trade
    latest_trade_datetime = datetime.utcfromtimestamp(trades[-1]['timestamp'] / 1e3)
    first_row = {'datetime': latest_trade_datetime, "trade_id": "now"}
    first_row['positions'] = get_active_derivative_positions(exchange)
    tradingperiod_list = [first_row]

    for i in range(len(trades)-1, -1, -1): # Loop backwards from latest trade
        try:
            trade = trades[i]
            trade_row_after = tradingperiod_list[-1]
            margin_symbol = trade['info']['marginAsset']
            base_asset_amount = trade['amount']
            traded_symbol = trade['symbol']
            if trade['side'] =='buy':
                position_delta = -trade['amount']
            elif trade['side'] == 'sell':
                position_delta = trade['amount']
            else:
                raise ValueError("trade side is not buy or sell")

            trade_datetime = datetime.utcfromtimestamp(trade['timestamp']/1e3)
            if not traded_symbol in trade_row_after["positions"]:
                trade_row_after["positions"][traded_symbol] = 0.0

            position_amount_trade_before = trade_row_after["positions"][traded_symbol] + position_delta
            positions_dict_trade_before = { traded_symbol: position_amount_trade_before }

            trade_row_before = copy.copy(trade_row_after)
            trade_row_before.update({'datetime': trade_datetime, 'trade_id': trade['id'], 'positions': positions_dict_trade_before})
            tradingperiod_list.append(trade_row_before)

        except Exception as e:
            print("Error occured on trade no ", i, ", trade is: ")
            print("trade_row_after: ", trade_row_after)
            display(trade)
            raise e

    tradingperiod_list[-1]['realized_cumpnl'] = 0.0
    for i in range(len(trades) - 1, -1, -1): # Loop backwards (forward in time)
        try:
            trade = trades[i]
            trade_realized_pnl = float(trade['info']['realizedPnl'])
            tradingperiod_list[i]['realized_cumpnl'] = tradingperiod_list[i+1]['realized_cumpnl'] + trade_realized_pnl
        except Exception as e:
            print("Error occured on trade no ", i, ", trade is: ")
            print("trade_row_after: ", trade_row_after)
            display(trade)
            raise e

    return pd.DataFrame(tradingperiod_list)

def earliest_row(tradingperiod_df: pd.DataFrame):
    return tradingperiod_df.iloc[-1]

def latest_row(tradingperiod_df: pd.DataFrame):
    return tradingperiod_df.iloc[0]

def print_trading_summary(exchange, tradingperiod_df: pd.DataFrame, base_symbol: str, quote_symbol: str, is_derivative):
    print("Analyzing PNL for ", base_symbol, " quoted in ", quote_symbol, " , is_derivative: ", is_derivative)
    # Print : Total PNL (absolute and relative), trades count, trading period length, max drawdown
    if is_derivative:
        ticker = base_symbol
        current_price = 0 # Zero for derivatives
        print("Current price of ", ticker, " is ", np.float64(exchange.fetchTicker(ticker)['info']['price']),
              ", although it will not be included in PNL calculation (as a derivative)")
    else:
        ticker = base_symbol + "/" + quote_symbol
        current_price = np.float64(exchange.fetchTicker(ticker)['info']['price'])
        print("Current price of ", ticker, " is ", current_price)

    # Current price is used, meaning hold is assumed for PNL calcol]
    ending_value_in_quote = latest_row(tradingperiod_df)[quote_symbol] + latest_row(tradingperiod_df)[base_symbol]*current_price
    starting_value_in_quote = earliest_row(tradingperiod_df)[quote_symbol] + earliest_row(tradingperiod_df)[base_symbol]*current_price

    pnl_absolute = ending_value_in_quote - starting_value_in_quote
    pnl_relative_pct = (pnl_absolute / starting_value_in_quote) * 100
    print("PNL: Absolute: %.3f" % pnl_absolute, " ", quote_symbol, " \t Relative: %.2f" % pnl_relative_pct, " %")

def analyze_pnl_since(exchange, filtered_symbol: str, quote_symbol: str, date_since_isostring: str):
    filtered_trades = fetch_trades_since(exchange, filtered_symbol, exchange.parse8601(date_since_isostring))
    if len(filtered_trades) == 0:
        print("ERROR: No trades fetched for ", filtered_symbol)
        return
    current_balances = fetch_balances_for_single_pair(exchange, filtered_symbol, quote_symbol)

    tradingperiod_list = backpropagate_trades(current_balances, filtered_trades, quote_symbol, filtered_symbol)
    tradingperiod_df = pd.DataFrame(tradingperiod_list)

    starting_quote_balance = tradingperiod_df[quote_symbol].iloc[-1]
    tradingperiod_df['trade_pnl'] = tradingperiod_df[quote_symbol] - tradingperiod_df[quote_symbol].shift(-1)
    tradingperiod_df['total_pnl'] = tradingperiod_df[quote_symbol] - starting_quote_balance

    # Temporary: Save to CSV
    filename = '/Users/alan/Downloads/last_analyzed_trades.csv'
    if tradingperiod_df.to_csv(filename) is None:
        print("Saved CSV into location: ", filename)


    display(tradingperiod_df)
    fig = tradingperiod_df.plot("datetime", quote_symbol) # Print quote symbol vs time (equity curve)
    fig.show()

    is_derivative = '/' not in filtered_symbol
    print_trading_summary(exchange, tradingperiod_df, filtered_symbol, quote_symbol, is_derivative)

def analyze_account(exchange, date_since_isostring: str):
    trades = fetch_trades_since(exchange, None, exchange.parse8601(date_since_isostring))
    current_balances = fetch_all_balances(exchange)

    tradingperiod_df = backpropagate_account_state(current_balances, trades)
    return tradingperiod_df

if __name__ == '__main__':
    pd.options.plotting.backend = "plotly"
    print("Executing __main__ section:")