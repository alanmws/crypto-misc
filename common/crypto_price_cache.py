import ccxt
import time

import pandas as pd

# Cache of uncderlying crypto prices
class CryptoPriceCache:
    def __init__(self):
        self._pricing_cache = {}

    def fetch_binance_ohlcv(self, symbol, timeframe, start_time, end_time):
        """
        Fetch OHLCV data of a crypto symbol from Binance within a specified period of time.

        Returns:
        - DF of OHLCV data points.
        """
        binance = ccxt.binance()
        ohlcv = []
        current_time = start_time

        limit = 1000 # Limit per request

        while current_time < end_time:
            try:
                # Fetch OHLCV data
                data = binance.fetch_ohlcv(symbol, timeframe, since=current_time, limit=limit)
                if not data:
                    break

                # Append fetched data to the result list
                ohlcv.extend(data)

                # Update current time to the end time of the fetched data
                current_time = data[-1][0] + (data[1][0] - data[0][0])  # Add the interval to move to the next timeframe

                # Sleep to avoid rate limiting
                time.sleep(binance.rateLimit / 1000)  # Convert milliseconds to seconds
            except Exception as e:
                print(f"Error fetching data: {e}")
                break

        df = pd.DataFrame(ohlcv, columns=['date', 'open', 'high', 'low', 'close', 'volume'])
        df['date'] = pd.to_datetime(df['date'], unit='ms')
        df.set_index('date', inplace=True)
        df = df.sort_index(ascending=True)

        self._pricing_cache[symbol] = df

        return df

    def get_ohlcv_from_timerange(self, symbol, start_time, end_time):
        symbol_prices = self._pricing_cache[symbol]
        return symbol_prices.loc[start_time:end_time]