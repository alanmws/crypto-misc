from datetime import datetime, timedelta, timezone
from tqdm import tqdm

import ast

import numpy as np
import time

import pandas as pd
import plotly.express as px

from decimal import Decimal
from dataclasses import dataclass
# from icecream import ic

from common.easy_options_data_access import EasyOptionsDataAccess
from common.crypto_price_cache import CryptoPriceCache

# ----------------------------------------------------------------------------------


@dataclass
class Trade:
    asset_key: str
    timestamp: float
    side: str  # 'buy' or 'sell'
    quantity: Decimal
    execution_price: Decimal = Decimal("0")
    fee: Decimal = Decimal("0")
    pnl: Decimal = None

@dataclass
class InventoryOption:
    asset_key: str
    quantity: Decimal = Decimal("0")
    avg_buy_price: Decimal = Decimal("0")

    def buy(self, quantity: Decimal, price: Decimal):
        new_quantity = self.quantity + quantity
        self.avg_buy_price = ((quantity * price) + (self.quantity * self.avg_buy_price)) / new_quantity
        self.quantity = new_quantity

    def sell(self, quantity: Decimal, price: Decimal) -> Decimal:
        self.quantity -= quantity
        pnl_per_contract = price - self.avg_buy_price
        sell_pnl = pnl_per_contract * quantity
        return sell_pnl


class EasyOptionMetadata:
    def __init__(self, option_id: str, strike_price: int):
        # Option ID must contain strike price substring
        if type(strike_price) is not int:
            strike_price = int(strike_price)
        assert str(strike_price) in option_id
        self.option_id = option_id
        self.strike_price = strike_price

    def __repr__(self):
        return f"({self.option_id} , strike: {self.strike_price})"

    def is_btc(self):
        return self.strike_price > 10e3

    def is_eth(self):
        return self.strike_price < 10e3

    def is_call(self):
        return 'CALL' in self.option_id

    def is_put(self):
        return 'PUT' in self.option_id

def last_time_of_30min_options_starting(current_time):
    # Calculate the number of minutes past the last 30-minute mark
    minutes_past_last_30 = current_time.minute % 30
    # Subtract the minutes to round down to the last 30-minute mark
    current_time -= timedelta(minutes=minutes_past_last_30)
    # Reset the seconds and microseconds to zero
    current_time = current_time.replace(second=0, microsecond=0)
    return current_time


def get_expected_settlement_time(current_time):
    # Find the nearest multiple of 30 minutes
    minutes = current_time.minute
    if minutes < 30:
        rounded_minutes = 30
    else:
        rounded_minutes = 0
        current_time += timedelta(hours=1)  # Move to the next hour if minutes >= 30

    # Create a new datetime with rounded minutes
    rounded_time = current_time.replace(minute=rounded_minutes, second=0, microsecond=0)
    return rounded_time


# Produces strings like '1600' for option expiring 16:00
def get_expected_settlement_time_marker(current_time):
    result_string = get_expected_settlement_time(current_time).strftime('%H%M')
    return result_string


def calculate_z_scored_volatility(df, n_intervals = 20):
    df['log_return'] = df['close'].pct_change().apply(lambda x: np.log(1 + x))
    rolling_std = df['log_return'].rolling(window=n_intervals).std()
    z_score = (rolling_std - rolling_std.mean()) / rolling_std.std()

    return z_score.iloc[-1] # Last value

class EasyOptionsData:
    def __init__(self, datastore: EasyOptionsDataAccess):
        self._datastore = datastore

    def get_available_options(self, current_time=None) -> (dict, dict):
        if current_time is None:
            current_time = datetime.now()
        end_tstamp = current_time.timestamp()
        start_tstamp = last_time_of_30min_options_starting(current_time).timestamp()
        option_price_dfs, strike_prices = self._datastore.get_options_data_from_timerange(start_tstamp, end_tstamp)

        # Filter out options that do not contain the right settlement time substring
        settlement_time_substring = get_expected_settlement_time_marker(current_time)
        option_price_dfs = {key: value for key, value in option_price_dfs.items() if settlement_time_substring in key}
        strike_prices = {key: value for key, value in strike_prices.items() if settlement_time_substring in key}

        return option_price_dfs, strike_prices

    def get_last_known_option_prices(self, current_time=None) -> (dict, dict):
        if current_time is None:
            current_time = datetime.now()
        end_tstamp = current_time.timestamp()
        start_tstamp = end_tstamp - 60 * 3  # -3min lookback

        return self._datastore.get_options_data_from_timerange(start_tstamp, end_tstamp)

    def get_last_known_option_price(self, option_id: str, current_time=None) -> float:
        if current_time is None:
            current_time = datetime.now()
        option_price_dataframes, _ = self.get_last_known_option_prices(current_time)

        return option_price_dataframes[option_id].iloc[-1]['price']

class EasyOptionsStrategy:
    UNDERLYING_SYMBOL = "BTC/USDT"  # Symbol of option underlying, used for price data access
    CHEAP_OPTION_MAX_PRICE = 0.15  # Threshold below which we consider an option as cheap and a candidate to buy
    SELLING_THRESHOLD_PRICE = 0.30  # Threshold at which we sell ('TP')
    MINIMAL_VOLALILITY_ZS = 0.0  # Minimal Z-Scored volatility to consider buying options

    def __init__(self, options_data: EasyOptionsData, price_cache: CryptoPriceCache):
        self._options_data = options_data
        self._crypto_price_cache = price_cache

        self.options_inventory = {}
        self.fiat_inventory = {}
        self.current_time: float = None
        self.trades = []
        self.inventory_history = {}  # Now represents just fiat inventory

        self.current_options_prices = {}

    def info(self, msg_str):
        print(f"INFO {self.get_current_datetime()}: {msg_str}")

    def get_current_datetime(self):
        return datetime.fromtimestamp(self.current_time, tz=timezone.utc)

    def get_current_option_price(self, option_id: str):
        return Decimal(self.current_options_prices[option_id].iloc[-1]['price'])

    def get_historical_inventory_df(self, asset: str):
        if asset not in self.inventory_history:
            return None
        return pd.DataFrame(self.inventory_history[asset], columns=['datetime', asset])

    def get_closed_trades(self):
        return [trade for trade in self.trades if trade.side == 'sell']

    def buy_option(self, option_id: str, contracts_count: int):
        if option_id not in self.options_inventory:
            self.options_inventory[option_id] = InventoryOption(option_id, 0)

        execution_price = self.get_current_option_price(option_id)
        self.options_inventory[option_id].buy(contracts_count, execution_price)

        total_cost = contracts_count * execution_price  # USDT
        self.fiat_inventory['USDT'] -= total_cost

        new_trade = Trade(option_id, self.current_time, "buy", contracts_count, Decimal(execution_price))
        self.trades.append(new_trade)

        self.info(f"Buying {contracts_count} contracts of option {option_id} at {execution_price}")

    def sell_or_settle_option(self, option_id: str, contracts_count: int, is_settlement: bool = False):
        assert option_id in self.options_inventory

        selling_fee = Decimal("0.05")  # NOTE Consider refactoring this fee calculation
        net_selling_price: Decimal = None
        if is_settlement:
            last_price = self._options_data.get_last_known_option_price(option_id, self.get_current_datetime())
            # Determine net settlement price
            net_selling_price = (Decimal("1.00") - selling_fee) if last_price > 0.5 else Decimal("0")
        else:  # Selling option
            net_selling_price = self.get_current_option_price(option_id) - selling_fee
            if net_selling_price < Decimal("0"):  # Selling price cannot be negative
                net_selling_price = Decimal("0")

        pnl = self.options_inventory[option_id].sell(contracts_count, net_selling_price)

        fiat_selling_value = net_selling_price * contracts_count
        self.fiat_inventory['USDT'] += fiat_selling_value
        self.inventory_history['USDT'].append((self.get_current_datetime(), self.fiat_inventory['USDT']))

        new_trade = Trade(option_id, self.current_time, "sell", contracts_count, Decimal(net_selling_price), selling_fee, pnl)
        self.trades.append(new_trade)

        sell_or_settle_str = "Selling" if is_settlement is False else "Settling"
        self.info(f"{sell_or_settle_str} {contracts_count} contracts of option {option_id} at {net_selling_price}, pnl = {pnl}")

    # Assesses the current option inventory and handles settlements
    def assess_inventory(self, options_price_dict: dict, options_metadata_dict: dict):
        # Sold options (quantity is zero); this is necessary to clear leftover option keys after selling
        sold_option_ids = [option_id for option_id, inventory_option in self.options_inventory.items()
                           if inventory_option.quantity == 0]

        settled_option_ids = []
        for option_id, inventory_option in self.options_inventory.items():
            if option_id not in options_price_dict:
                # Option no longer is available, suggesting settlement
                current_dt = self.get_current_datetime()
                if not (current_dt.minute == 30 or current_dt.minute == 0):
                    raise RuntimeError(f"Option in inventory - {option_id} - is no longer among available options at {current_dt}")

                # Settle option
                self.sell_or_settle_option(option_id, inventory_option.quantity, is_settlement=True)

                settled_option_ids.append(option_id)

        for option_id in settled_option_ids+sold_option_ids:
            self.options_inventory.pop(option_id)

        # Remove 0-quantity options (could have been sold by the strategy)
        self.options_inventory = {option_id: option_quantity for option_id, option_quantity in self.options_inventory.items() if option_quantity != 0}

    def strategy_event_impl(self, options_price_dict: dict, options_metadata_dict: dict):
        if len(self.options_inventory) == 0:  # Consider buying
            option_candidate_ids = []
            for option_id, options_prices in options_price_dict.items():
                last_price = options_prices.iloc[-1]['price']
                # print(f"Option {option_id} last price: {last_price}")
                option_metadata = options_metadata_dict[option_id]
                if last_price <= self.CHEAP_OPTION_MAX_PRICE and option_metadata.is_btc():
                    option_candidate_ids.append(option_id)

            calls_present = any([option_md.is_call() for option_id, option_md in options_metadata_dict.items() if option_id in option_candidate_ids])
            puts_present = any([option_md.is_put() for option_id, option_md in options_metadata_dict.items() if option_id in option_candidate_ids])

            # if calls_present and puts_present:
            #     self.info(f"Both cheap calls and puts are present, candidates: {option_candidate_ids}")

            # Get last 30min of price data
            recent_prices_ohlc = self._crypto_price_cache.get_ohlcv_from_timerange(self.UNDERLYING_SYMBOL, self.current_time - 30*60, self.current_time)
            actual_volatility_zs = calculate_z_scored_volatility(recent_prices_ohlc)

            if actual_volatility_zs < self.MINIMAL_VOLALILITY_ZS:
                return # Volatilty too low, don't buy

            call_option_ids = [option_id for option_id in option_candidate_ids if options_metadata_dict[option_id].is_call()]
            if calls_present:
                lowest_strike_call = call_option_ids[0]
                for option_id in call_option_ids:
                    last_lowest_strike = options_metadata_dict[lowest_strike_call].strike_price
                    strike_price = options_metadata_dict[option_id].strike_price
                    if strike_price < last_lowest_strike:
                        lowest_strike_call = option_id

                self.buy_option(lowest_strike_call, 10)

            put_option_ids = [option_id for option_id in option_candidate_ids if options_metadata_dict[option_id].is_put()]
            if puts_present:
                highest_strike_put = put_option_ids[0]
                for option_id in put_option_ids:
                    last_highest_strike = options_metadata_dict[highest_strike_put].strike_price
                    strike_price = options_metadata_dict[option_id].strike_price
                    if strike_price > last_highest_strike:
                        highest_strike_put = option_id

                self.buy_option(highest_strike_put, 10)

        else:  # There are options in the inventory, consider selling only
            for option_id, inventory_option in self.options_inventory.items():
                last_price = options_price_dict[option_id].iloc[-1]['price']
                if last_price >= self.SELLING_THRESHOLD_PRICE:
                    self.sell_or_settle_option(option_id, inventory_option.quantity)

    def strategy_event(self, current_time: float):
        self.current_time = current_time
        options_prices_dict, strike_prices = self._options_data.get_available_options(self.get_current_datetime())
        self.current_options_prices = options_prices_dict

        # Prepare options metadata
        options_metadata = {}
        for option_id, strike_price in strike_prices.items():
            metadata = EasyOptionMetadata(option_id, strike_price)
            options_metadata[option_id] = metadata

        try:
            self.assess_inventory(options_prices_dict, options_metadata)
        except:
            print(f"Exception occurred when assessing inventory at time: {self.get_current_datetime()}")
            raise

        try:
            self.strategy_event_impl(options_prices_dict, options_metadata)
        except:
            print(f"Exception occurred when running the strategy at time: {self.get_current_datetime()}")
            raise

    def backtest_options_strategy(self, start_time: datetime, end_time: datetime, interval_sec = 10):
        assert end_time > start_time
        starting_fiat_inventory = {"USDT": Decimal("100.0")}
        self.fiat_inventory = starting_fiat_inventory
        self.inventory_history['USDT'] = [(start_time, starting_fiat_inventory['USDT'])]

        print(f"Backtest will be done from {start_time} to {end_time}")
        start_ts = start_time.timestamp()
        end_ts = end_time.timestamp()
        # duration_sec = end_ts - start_ts

        self._crypto_price_cache.fetch_binance_ohlcv(self.UNDERLYING_SYMBOL, '1m', start_ts, end_ts)

        for current_ts in tqdm(np.arange(start_ts, end_ts, interval_sec)):
            self.strategy_event(current_ts)