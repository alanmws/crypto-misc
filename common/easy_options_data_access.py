from abc import ABC, abstractmethod

import pandas as pd


class EasyOptionsDataAccess(ABC):
    @abstractmethod
    def get_options_data_from_timerange(self, start_tstamp: int, end_tstamp: int) -> (pd.DataFrame, dict):
        pass