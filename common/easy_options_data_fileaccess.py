import json
import re
import pandas as pd

from datetime import datetime, timezone, timedelta
from typing import MutableMapping

from common.easy_options_data_access import EasyOptionsDataAccess


def convert_options_prices_lists_to_dataframes(options_prices: dict):
    options_price_dfs = {option_id: pd.DataFrame(pricing_list, columns=['datetime', 'price'])
                         for option_id, pricing_list in options_prices.items()}
    return options_price_dfs

def extract_strike_price(option_id_str: str):
        pattern = r'-(\d+)-'
        match = re.search(pattern, option_id_str)

        if match:
            price = int(match.group(1))
            return price
        else:
            return None

def process_eoptions_json(json_dict: dict,
                          start_tstamp: int,
                          end_tstamp: int,
                          option_prices: MutableMapping[str, list],
                          strike_prices: MutableMapping[str, int]):
    MINIMAL_VALID_PRICE = 0.05  # Minimum valid price for options, anything below is skipped

    if json_dict['type'] != 'instrument.price':
        return None
    try:
        datafields = json_dict['data']
        if len(datafields) == 0:
            return None
        timestamp = datafields[0]['t'] / 1000  # ms -> s
        if timestamp < start_tstamp or timestamp > end_tstamp:
            return  # outside requested time range
        # option_tuple = (timestamp, datafields)
        # self._data_cache.append(option_tuple)

        pricing_datetime = datetime.fromtimestamp(timestamp, tz=timezone.utc)
        for datafield in datafields:
            option_id = datafield['i']
            option_price = datafield['sp']
            strike_price = extract_strike_price(option_id)
            if option_id not in strike_prices:
                strike_prices[option_id] = strike_price

            if option_id not in option_prices:
                option_prices[option_id] = []
            if option_price < MINIMAL_VALID_PRICE:
                continue
            option_prices[option_id].append([pricing_datetime, option_price])
    except Exception as e:
        print(f"Exception, json_dict = {json_dict}")
        raise

class EasyOptionsDataFileAccess(EasyOptionsDataAccess):
    def __init__(self, filename):
        self._data_filename = filename
        self._cached_option_prices = None
        self._cached_strike_prices = None

    def fetch_options_pricing_data_from_file(self, start_tstamp: int, end_tstamp: int,
                                             limit: int = 1000 * 1000) -> (dict, dict):
        self._cached_option_prices = {}
        self._cached_strike_prices = {}

        i = 0
        with open(self._data_filename, 'r') as file:
            for line in file:
                if i >= limit:
                    break
                i += 1

                # Find the position of the first '{' character
                start_index = line.find('{')
                if start_index == -1:
                    continue  # No JSON at that line
                # Extract the JSON portion from the line
                json_str = line[start_index:]
                try:
                    data = json.loads(json_str)
                    process_eoptions_json(data, start_tstamp, end_tstamp, self._cached_option_prices, self._cached_strike_prices)
                except json.JSONDecodeError as e:
                    print("Error decoding JSON:", e)
                    continue

        self._cached_option_prices = convert_options_prices_lists_to_dataframes(self._cached_option_prices)

    # # Implements EasyOptionsDataAccess interface method
    # def get_options_data_from_timerange(self, start_tstamp: int, end_tstamp: int):
    #     option_prices, strike_prices = self.get_options_pricing_data_from_file(self._data_filename, start_tstamp, end_tstamp)
    #
    #     options_price_dataframes = convert_options_prices_lists_to_dataframes(option_prices)
    #     return options_price_dataframes, strike_prices

    # Implements EasyOptionsDataAccess interface method
    def get_options_data_from_timerange(self, start_tstamp, end_tstamp) -> (pd.DataFrame, dict):
        filtered_options_dict = {}
        start_datetime = pd.to_datetime(start_tstamp, unit='s', utc=True)
        end_datetime = pd.to_datetime(end_tstamp, unit='s', utc=True)

        for key, df in self._cached_option_prices.items():
            # Filter dataframe rows within the specified time range
            mask = (df['datetime'] >= start_datetime) & (df['datetime'] <= end_datetime)
            filtered_df = df[mask]

            if len(filtered_df) > 0:
                filtered_options_dict[key] = filtered_df

        # Filter out cached strike prices to only contain options available at the requested time
        option_ids_available = filtered_options_dict.keys()
        strike_prices = {key: self._cached_strike_prices[key] for key in option_ids_available if key in self._cached_strike_prices}

        return filtered_options_dict, strike_prices

