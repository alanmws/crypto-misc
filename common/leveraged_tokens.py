import ccxt
import pickle
import re
import os

import pandas as pd
import numpy as np

from datetime import datetime, timedelta

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException

from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor

import logging
import concurrent

logger = logging.getLogger(__name__)

# returns list of 3-element lists containig tickers split like: ['BTC', '3S', 'USDT']
def get_all_mexc_lvg_token_tickers():
    exchange = ccxt.mexc() # NOTE format for those tokens differs between mexc and mexc3
    tickers = []
    markets = exchange.fetch_markets()
    for market in markets:
        tickers.append(market['id'])

    results = []
    ltoken_regexp = re.compile(r"(\w*)([3][L|S])\_(\w*)") # NOTE: Only leverage 3 supported now, this can be easily extended
    for token in tickers:
        match = ltoken_regexp.search(token)
        if match:
            token = match.group(1)  # ie. BTC
            leverage = match.group(2)  # ie. 3S
            quote_token = match.group(3)  # ie. USDT
            result_entry = [token, leverage, quote_token]
            results.append(result_entry)

    return results

# like get_all_mexc_lvg_token_tickers but returns DataFrame
def get_all_mexc_lvg_token_tickers_df():
    all_lvg_mexc_tickers = get_all_mexc_lvg_token_tickers()
    all_lvg_mexc_tickers_df = pd.DataFrame(all_lvg_mexc_tickers, columns=["ticker", "leverage", "quote_ticker"])
    return all_lvg_mexc_tickers_df

# returns a ccxt-supported format of token USDT perpetual
def ticker_to_mexc_usdt_perp(ticker: str):
    return ticker+"/USDT:USDT"

####################################################################################################################

class LeveragedTokensInfoHandler():
    cachedir = "/Users/alan/Documents/algotrading/python_workspace/rebalances_data_cache"

    def __init__(self):
        if not os.path.isdir(LeveragedTokensInfoHandler.cachedir):
            os.mkdir(LeveragedTokensInfoHandler.cachedir)
            logging.info(f"Created cache directory: {LeveragedTokensInfoHandler.cachedir}")

    def _file_for_ticker(self, ticker: str):
        file_suffix = ticker + "_MEXC"
        return self.cachedir + "/rebalances_" + file_suffix + ".csv"

    def _read_datafile(self, ticker: str):
        file_loc = self._file_for_ticker(ticker) # This method returns just a string with full expected location of tje .csv file
        try:
            return pd.read_csv(file_loc, parse_dates=['datetime'])
        except:
            return None

    def get_rebalances(self, ticker: str):
        return self._read_datafile(ticker)

    def _save_or_extend_rebalance_data(self, ticker: str, rebalance_history: pd.DataFrame):
        file_loc = self._file_for_ticker(ticker)

        if os.path.isfile(file_loc):
            existing_data = self._read_datafile(ticker)
            # Concatenate the DataFrames
            merged_df = pd.concat([existing_data, rebalance_history])
            # Remove duplicates (keep the first occurrence)
            merged_df = merged_df.loc[~merged_df.datetime.duplicated(keep="first")]
            merged_df.sort_values(by='datetime', inplace=True)
            merged_df.reset_index(drop=True, inplace=True)
            merged_df.to_csv(file_loc, index=False)
            logger.info(f"Merged and saved data to {file_loc}")
        else:
            rebalance_history.to_csv(file_loc, index=False)
            logger.info(f"Saved new data to {file_loc}")

    def _ticker_website_string_to_ticker(self, input_str: str):
        # Find the pattern of interest
        match = re.search(r"(\w+\d+)\s+[Xx]\s+(\w)", input_str)

        if match:
            # Extract the two matched groups and concatenate them
            result = match.group(1) + match.group(2).upper()
            return result
        else:
            return None

    # returns row representing latest rebalance for a token, as long as it's no later than max_time_passed
    def get_latest_rebalance_info(self, token, assumed_time_now = None, max_time_passed = timedelta(hours=24, minutes=1)):
        if assumed_time_now is None:
            assumed_time_now = datetime.now()
        token_rebalances = self._read_datafile(token)
        if token_rebalances is None:
            return None
        past_rebalances = token_rebalances[token_rebalances["datetime"] <= assumed_time_now]
        if len(past_rebalances) == 0:
            return None

        latest_rebalance = past_rebalances.loc[past_rebalances['datetime'].idxmax()]
        time_elapsed = assumed_time_now - latest_rebalance['datetime']
        logger.debug(f"DBG time_elapsed: {time_elapsed}, time_now: {assumed_time_now}, rebal at: {latest_rebalance['datetime']}")
        if time_elapsed > max_time_passed:
            logger.info(f"get_latest_rebalance_info: Rebalance too old, happened {time_elapsed} ago")
            return None
        return latest_rebalance

    @staticmethod
    def extract_rebalances_dataframe(row_elements):
        rows_processed = []
        for row in row_elements:
            cells = row.find_elements('tag name', 'td')
            row_data = [cell.text for cell in cells]
            if len(row_data) < 4:
                continue  # skip empty
            date_valid = True

            try:
                timestamp = datetime.strptime(row_data[0], '%Y-%m-%d %H:%M:%S')
            except:
                date_valid = False

            if not date_valid:
                continue  # skip empty/invalid row

            row_data[0] = timestamp

            try:
                row_data[2] = float(row_data[2].replace(',', ''))  # delta
            except:
                row_data[2] = np.nan

            rows_processed.append(row_data)

        rebalances_df = pd.DataFrame(rows_processed, columns=["datetime", "before_after", "delta", "lvg_before_after",
                                                              "rebalance_price"])
        return rebalances_df

    def get_rebalances_of_mexc_ticker(self, ticker: str, driver=None):

        # Set up the webdriver
        close_driver_at_finalization = False
        if driver is None:
            driver = webdriver.Chrome()
            close_driver_at_finalization = True

        url = "https://www.mexc.com/leveraged-ETF/" + ticker

        try:
            driver.get(url)

            # Wait for the dynamically generated content to load
            wait = WebDriverWait(driver, 10)
            table_element = wait.until(EC.visibility_of_element_located(
                (By.XPATH, '//*[@id="__next"]/div[2]/div/div[2]/div/div/div/div/div/div/table')))

            ticker_element = driver.find_element(By.XPATH, '//*[@id="__next"]/div[2]/section/div[1]/div[1]/div[2]/div[1]')
            ticker_on_website = self._ticker_website_string_to_ticker(ticker_element.text)
            if ticker_on_website != ticker:
                # If this condition occurs, erroneous data would have been loaded
                logging.warning(f"Mismatch between ticker expected: {ticker} and ticker on the website: {ticker_on_website}")
                driver.save_screenshot(f"screenshot_mismatch_{ticker}_vs_{ticker_on_website}.png")
                return None

            wait.until(lambda element: len(element.find_elements('tag name', 'tr')) > 4)
            row_elements = table_element.find_elements('tag name', 'tr')
            assert len(row_elements) > 4

            return self.extract_rebalances_dataframe(row_elements)

        except TimeoutException:
            logging.info(f"Timeout when scraping: {ticker}")

        except Exception as exc:
            logging.info(f"Scraping failed on {ticker} (exception thrown): {exc}", exc_info=True)

        finally:
            # Close the webdriver
            if close_driver_at_finalization:
                driver.quit()

    def fetch_rebalances_for_all_tokens(self, must_contain_str: str = ""):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(options=chrome_options)

        all_lvg_mexc_tickers = get_all_mexc_lvg_token_tickers()

        failed_tickers = []
        new_tickers = []
        for ticker_info in tqdm(all_lvg_mexc_tickers):
            ticker_for_url = ticker_info[0] + ticker_info[1]
            if must_contain_str and must_contain_str not in ticker_for_url:
                continue
            try:
                rebalance_history = self.get_rebalances_of_mexc_ticker(ticker_for_url, driver)
                new_tickers.append(ticker_for_url)
                self._save_or_extend_rebalance_data(ticker_for_url, rebalance_history)
            except:
                failed_tickers.append(ticker_for_url)

        logger.info(f"Added data for tickers: {new_tickers}")
        logger.info(f"Failed to retrieve data for tickers: {failed_tickers}")

    def fetch_rebalances_for_token(self, token: str):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(options=chrome_options)

        try:
            rebalance_history = self.get_rebalances_of_mexc_ticker(token, driver)
            if rebalance_history is None:
                return False
            self._save_or_extend_rebalance_data(token, rebalance_history)
        except:
            logging.info(f"Failed to fetch rebalances for token: {token}")
            raise

        return True

    def fetch_rebalances_for_all_tokens_parallel(self):
        results = [] # TODO consider if this is necessary here, now results is unused

        all_lvg_mexc_tickers = get_all_mexc_lvg_token_tickers()
        tickers_for_url = [ticker_info[0] + ticker_info[1] for ticker_info in all_lvg_mexc_tickers]

        with ThreadPoolExecutor(max_workers=5) as executor:
            futures = {executor.submit(self.fetch_rebalances_for_token, urlticker): urlticker for urlticker in tickers_for_url}

            for future in tqdm(concurrent.futures.as_completed(futures), total=len(futures)):
                urlticker = futures[future]
                try:
                    data = future.result()
                    results.append(data)
                except Exception as e:
                    logging.error(f"Error while scraping {urlticker}: {e}")

        return results