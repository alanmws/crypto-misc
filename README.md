# crypto-misc

* easy_options_example.ipynb - Example of running AscendEX easy options strategy in Novemver of last year. Those options were priced from 0.05 to 1.00 USD and settled every 30min. In periods of increased volatility, they were often quite mispriced and you could buy them underpriced.
This is not a cherrypicked day, I chose this day because only since this day (for a few days) I have a websocket data log containing all the pricing data for those options (this is websocket that the exchange used to communicate with their fronted). I actually used Redis and InfluxDB for this data generally but that would require running instances of those services to even view this pricing data.

The Easy Options product was shut down by the exchange in Dec 2023, I think perhaps because they were bleeding money on this. Surely not only I discovered those mispricings.

* leveraged_tokens.py - Most of code I used for analyzing and playing with MEXC leveraged tokens, like I mentioned on the call

* enter_event_move.py - A bit messy code that I used to run a strategy (that I forgot to mention during our call) of entering long or short on important market info releases like US CPI info release. Getting in the first second from the release was getting nice results with good R:R ratio as long as the released numbers at least slightly diverged from market expectations. Closing of the trade was usually manual. I played this primarily in 2022 and a bit in 2023.
Example of running this strategy is from the wiki screenshot.

* wiki_screenshot_example_CPI play.png - Example of running the above strategy regarding CPI release. Note this is something totally unfiltered that I write for myself. Shows an example of good automated trade that I then partially screwed up manually.

./common:
* easy_options.py - contains easy options strategy and backtesting code
* easy_options_data_fileaccess.py - File I made specifically to be able to access option prices from websocket text log
