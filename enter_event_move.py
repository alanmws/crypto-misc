import logging
import sys
import time

from trade_risk_calculator import FuturesDirectionalTradeParams, fill_position_size
from exchanges import exchange_from_str

from decimal import Decimal
from datetime import datetime, timezone, timedelta
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.schedulers.background import BackgroundScheduler

from cryptofeed import FeedHandler
from cryptofeed.defines import TRADES
from cryptofeed.exchanges import BinanceFutures
from cryptofeed.backends.aggregate import CustomAggregate

import numpy as np
import ccxt

from pytz import timezone

# GLOBAL VARS
dominating_side = None

# gets current midprice
def get_current_price(symbol: str, exchange: ccxt.Exchange) -> Decimal:
    orderbook = exchange.fetch_order_book(symbol)
    bid = orderbook['bids'][0][0] if len(orderbook['bids']) > 0 else None
    ask = orderbook['asks'][0][0] if len(orderbook['asks']) > 0 else None
    #spread = (ask - bid) if (bid and ask) else None
    current_price = ((bid+ask)/2) if (bid and ask) else None
    return Decimal(current_price)

def execute_trade(trade_params: FuturesDirectionalTradeParams, exchange: ccxt.Exchange , side):
    print("Executing trade: ", trade_params)
    fulL_symbol = trade_params.base_asset + '/' + trade_params.quote_asset

    # print("Exec skipped, test mode")
    try:
        order = exchange.create_order(fulL_symbol, 'MARKET', side, trade_params.base_amount)
        print(order)

        inverted_side = 'sell' if side == 'buy' else 'buy'

        # stopLossParams = {'stopPrice': trade_params.stop_loss_price,
        #                   'reduceOnly': True }
        # stopLossOrder = exchange.create_order(fulL_symbol, 'STOP_MARKET', inverted_side, trade_params.base_amount, None, stopLossParams)
        # print(stopLossOrder)

        # takeProfitParams = {'stopPrice': trade_params.take_profit_price,
        #                     'reduceOnly': True }
        # takeProfitOrder = exchange.create_order(fulL_symbol, 'TAKE_PROFIT_MARKET', inverted_side, trade_params.base_amount, None,
        #                                         takeProfitParams)
        # Limit order as TP

        order_info = order['info']
        print("order['status']  = ", order_info['status'] )
        print(order_info)
        if order_info['status'] == 'FILLED' or 'closed':
            avg_fill_price = Decimal(order_info['avgPrice'])
        else:
            print("ERROR: Order not filled")
            return

        position_size_quote = trade_params.base_amount * avg_fill_price
        target_profit_quote = position_size_quote * Decimal("1.0")
        leverage = Decimal("100")
        target_price_long = (avg_fill_price + target_profit_quote/leverage)
        target_price_short = (avg_fill_price - target_profit_quote/leverage)
        # trade_params.take_profit_price = target_price_long if is_long else target_price_short


        # takeProfitParams = { 'reduceOnly': True }
        # takeProfitOrder = exchange.create_order(fulL_symbol, 'LIMIT', inverted_side, trade_params.base_amount, trade_params.take_profit_price,
        #                                         takeProfitParams)
        # print(takeProfitOrder)

    except Exception as e:
        print(type(e).__name__, str(e))
        raise

def execute_trade_limit(symbol: str, side: str, amount: Decimal, limit_price: Decimal, sl_price: Decimal, tp_price: Decimal, exchange: ccxt.Exchange):
    # fulL_symbol = trade_params.base_asset + '/' + trade_params.quote_asset
    fulL_symbol = symbol

    # print("Exec skipped, test mode")
    try:
        order = exchange.create_order(fulL_symbol, 'LIMIT', side, amount, price=limit_price)
        print(order)

        order_info = order['info']
        print("order['status']  = ", order_info['status'] )
        print(order_info)

        timeout_time = time.time()+60
        while True:
            if order_info['status'] == 'FILLED' or 'closed':
                avg_fill_price = Decimal(order_info['avgPrice'])
                print(f"Order filled at price {avg_fill_price}")
                break
            else:
                time.sleep(0.1)
                if time.time() > timeout_time:
                    print("ERROR: Limit order timed out.")
                    return

        inverted_side = 'sell' if side == 'buy' else 'buy'

        stopLossParams = {'stopPrice': sl_price,
                          'reduceOnly': True }
        stopLossOrder = exchange.create_order(fulL_symbol, 'STOP_MARKET', inverted_side, amount, None,
                                              stopLossParams)
        print(stopLossOrder)

        takeProfitParams = {'stopPrice': tp_price,
                            'reduceOnly': True }
        takeProfitOrder = exchange.create_order(fulL_symbol, 'LIMIT', inverted_side, amount, None,
                                                takeProfitParams)
        print(takeProfitParams)

    except Exception as e:
        print(type(e).__name__, str(e))
        raise

def enter_event_trade(symbol: str, exchange: ccxt.Exchange, risked_amount_quote: Decimal, is_long: bool, current_price: Decimal, leverage: Decimal):
    # current_price = get_current_price(symbol, exchange)

    # At the moment, take profit and stop loss set to None
    trade_params: FuturesDirectionalTradeParams = FuturesDirectionalTradeParams("BTC", "USDT", (risked_amount_quote/current_price)*leverage, current_price, None, None)

    side = 'buy' if is_long else 'sell'
    execute_trade(trade_params, exchange, side)

def execute_funding_play(symbol: str, exchange: ccxt.Exchange, risked_amount_quote: Decimal):
    current_price = get_current_price(symbol, exchange)
    limit_price = current_price * 1.0005
    # We will be short-only
    take_profit_price = (1 - (0.6e-2*2.5)) * limit_price
    stop_loss_price = (1 + (0.6e-2*0.5)) * limit_price
    execute_trade_limit(symbol, "sell", limit_price, stop_loss_price, take_profit_price, exchange)

class WindowedTradeAnalyzer():
    def __init__(self, trade_params: dict, trade_timestamp, leverage: Decimal):
        timestamp_utc_now = time.time()
        print(f"DBG: target_trade_time.timestamp(): {target_trade_time.timestamp()}, timestamp_utc_now: {timestamp_utc_now}")
        time_left_to_trade = trade_timestamp - timestamp_utc_now
        if time_left_to_trade < 0:
            print(f"ERROR: Timestamp of trade == {trade_timestamp} is in the past")
            raise RuntimeError(f"ERROR: Timestamp of trade == {trade_timestamp} is in the past")
        self.trade_params = trade_params
        self.trade_timestamp = trade_timestamp
        self.window_duration = 0.5  # seconds
        self.trades_in_window = []
        self.trade_triggered = False
        self.leverage = leverage

        print(f"Created new trade scheduled to trade in {time_left_to_trade} sec.")

    def analyze_window_and_enter_trade(self):
        buy_vol = Decimal("0")
        sell_vol = Decimal("0")
        for trade in self.trades_in_window:
            if trade.side == 'buy':
                buy_vol += trade.amount
            elif trade.side == 'sell':
                sell_vol += trade.amount
            else:
                print("ERROR: Unknown side of trade: ", trade.side)

        last_price = self.trades_in_window[-1].price
        is_long = buy_vol > sell_vol # NOTE perhaps add minimal diff (?)

        self.trade_triggered = True
        enter_event_trade(self.trade_params['symbol'], self.trade_params['exchange'], self.trade_params['risked_amount_quote'],
                          is_long, last_price, self.leverage)

    async def trade(self, t, receipt_timestamp):
        # print(f"Trade received at {receipt_timestamp}: {t}")
        # print("Trade:", t)
        self.trades_in_window.append(t)
        earliest_window_time = t.timestamp - self.window_duration

        total_win_duration = self.trades_in_window[-1].timestamp - self.trades_in_window[0].timestamp
        # if len(self.trades_in_window) >= 10:
        #     for trade in self.trades_in_window:
        #         print("P trade: ", trade)
        for trade in self.trades_in_window:
            if trade.timestamp < earliest_window_time:
                self.trades_in_window.remove(trade)
            # else:
            #     break # we can stop as trades are sorted

        if not self.trade_triggered and receipt_timestamp >= self.trade_timestamp:
            print(f"Target timestamp of {self.trade_timestamp} reached with trade ts {receipt_timestamp}, triggering trade")
            self.analyze_window_and_enter_trade()

        # print("Window length: ", len(self.trades_in_window), ", duration: ", total_win_duration)

async def callback(data):
    if 'BTC-USDT-PERP' not in data:
        return
    volumes_dict = data['BTC-USDT-PERP']
    volumes_diff = volumes_dict['buy_vol'] - volumes_dict['sell_vol']
    volume_total = volumes_dict['buy_vol'] + volumes_dict['sell_vol']
    global dominating_side
    if abs(volumes_diff) / volume_total < 0.1:
        dominating_side = None
    else:
        dominating_side = 'buy' if volumes_diff > Decimal('0') else 'sell'
    print(data, "; Dominating side: ", dominating_side)

def custom_agg(data, trade, receipt):
    print("Trade:" , trade)
    if trade.symbol not in data:
        data[trade.symbol] = {'buy_vol' : Decimal("0"), 'sell_vol' : Decimal("0")}
    if trade.side == 'buy':
        data[trade.symbol]['buy_vol'] += trade.amount
    elif trade.side == 'sell':
        data[trade.symbol]['sell_vol'] += trade.amount
    else:
        print("ERROR: Unknown side of trade: ", trade.side)


def init(data):
    """
    called at start of each new interval. We just need to clear the
    internal state
    """
    data.clear()

def test_trade_imminent(crypto_feed):
    # trade_time = datetime.now() + timedelta(seconds=5)
    trade_time = datetime(2023, 3, 22, 19, 00, 3, 0*1000)
    trade_params = {'symbol': 'BTC-USDT-PERP', 'exchange': exchange_from_str("binance"), 'risked_amount_quote': Decimal("5")}
    analyzer = WindowedTradeAnalyzer(trade_params, trade_time.timestamp())
    crypto_feed.add_feed(BinanceFutures(symbols=['BTC-USDT-PERP'], channels=[TRADES], callbacks={TRADES: analyzer.trade}))

    print("FeedHandler is being started..")
    crypto_feed.run()

if __name__ == "__main__":
    mandatory_args = {"exchange", "symbol", "risked_amount_quote"}
    args = {}
    for arg in sys.argv[1:]:
        name_and_val = arg.split("=")
        if len(name_and_val) != 2:
            print("Invalid arg:", arg)
            pass
        args[name_and_val[0]] = name_and_val[1]

    for mandatory_arg in mandatory_args:
        if mandatory_arg not in args:
            print("Missing mandatory arg: ", mandatory_arg)

    exchange = exchange_from_str(args["exchange"])
    symbol = args["symbol"]
    risked_amount_quote = Decimal(args["risked_amount_quote"])

    # Exchange settings
    leveraged_used = 20
    exchange.setMarginMode("isolated", symbol = "BTC/USDT", params = {"leverage", leveraged_used})
    # exchange.setMarginMode("isolated", params={"leverage", leveraged_used})

    target_trade_time = datetime(2023, 9, 13, 12, 24, 1, 0*1000, tzinfo=timezone('UTC'))
    sched = BackgroundScheduler()
    job = sched.add_job(test_trade_imminent, 'date', run_date=target_trade_time, args=[symbol, exchange, risked_amount_quote])
    print("Job added: ", job)

    print(BinanceFutures.symbols())

    config = {'log': {'filename': 'demo.log', 'level': 'DEBUG', 'disabled': False}}
    # the config will be automatically passed into any exchanges set up by string. Instantiated exchange objects would need to pass the config in manually.
    f = FeedHandler(config=config)
    # f.add_feed(BinanceFutures(symbols=['BTC-USDT-PERP'], channels=[TRADES], callbacks={TRADES: CustomAggregate(callback, window=1, init=init, aggregator=custom_agg)}))

    # test_trade_imminent(f)

    print("Scheduler is being started..")
    sched.start()

